<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    <title>UTS PEMOGRAMAN WEB</title>
</head>

<body>
    <nav class="navbar navbar-light bg-info">
        <div class="container-fluid">
            <span class="navbar-brand align-left text-left mb-2 h1">FARHAN DWIMA SURYA</span>
            <span class="navbar-brand align-right text-right mb-2 h1">UTS PWEB</span>
        </div>
    </nav>

    <div class="container">
        <h1 class=" mt-3 mb-4 text-center">Tambah Data</h1>

        <form>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" required="required" id="nama" name="nama" class="form-control mb-4"
                    placeholder="Masukkan Nama">
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" required="required" id="alamat" name="jml" class="form-control mb-4"
                    placeholder="Masukkan Jumlah Barang">
            </div>
            <div class="form-group">
                <label for="usia">Usia</label>
                <input type="text" required="required" id="Usia" name="tgl_beli" class="form-control mb-4"
                    placeholder="Masukkan Usia">
            </div>
            <div class="form-group">
                <label for="harga">Harga</label>
                <input type="text" required="required" id="harga" name="hrg_ttl" class="form-control mb-4"
                    placeholder="Masukkan Harga">
            </div>
            <div class="text-center">
                <a href="index.html" class="btn-warning btn-lg" type="submit">Submit</a>
            </div>
        </form>
    </div>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
</body>
</html>